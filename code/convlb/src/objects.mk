# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2018, 2023 NXP
#
#==============================================================================
#
#    File Name:  objects.mk
#
#    General Description: Defines the object files for the api layer
#
#==============================================================================

# List the api object files to be built
OBJECTS += \
    convlb.o

OBJECTS_CONVLB += \
    convlb.o

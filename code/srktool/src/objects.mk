# SPDX-License-Identifier: BSD-3-Clause
#
# Freescale Semiconductor
# (c) Freescale Semiconductor, Inc. 2011. All rights reserved.
# Copyright 2023 NXP
#
#==============================================================================
#
#    File Name:  objects.mk
#
#    General Description: Defines the object files for srk tool
#
#==============================================================================

# List the api object files to be built
OBJECTS += \
    srktool.o

OBJECTS_SRKTOOL += \
    srktool.o
